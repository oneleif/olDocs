# oneleif Active Projects
* [Metal Game Engine](https://github.com/2etime/Dark-Engine)

    Creating a badass Game Engine from scratch using Metal and Swift.
    
* [kyy](https://github.com/parshav/kyy)

    An experimental project for playing around with Kotlin.

* [SwiftObject](https://gitlab.com/_leif/SwiftObject)

    SwiftObject is a swift implementation of JS-type Object creation with dynamic properties.
    


## CocoaPods
* [SwiftObject](http://cocoapods.org/pods/SwiftObject)

## oneleif Discord Link
[Discord](https://discord.gg/wkekGe3)

<!-- Please don't remove this: Grab your social icons from https://github.com/carlsednaoui/gitsocial -->

[1.1]: http://i.imgur.com/tXSoThF.png (twitter icon with padding)
[2.1]: http://i.imgur.com/P3YfQoD.png (facebook icon with padding)
[3.1]: http://i.imgur.com/yCsTjba.png (google plus icon with padding)
[4.1]: http://i.imgur.com/YckIOms.png (tumblr icon with padding)
[5.1]: http://i.imgur.com/1AGmwO3.png (dribbble icon with padding)
[6.1]: http://i.imgur.com/0o48UoR.png (github icon with padding)